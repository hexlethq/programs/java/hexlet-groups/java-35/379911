package exercise;

class App {
    public static void numbers() {
        // BEGIN
        int x = 8;
        int y = 2;
        int z = 100;
        int s = 3;
        int result = x / y + z % s;
        System.out.println(result);
        // END
    }

    public static void strings() {
        String language = "Java";
        // BEGIN
        String action = " works on";
        String compiller = "JVM";
        String result =  language + action + compiller;
        System.out.println(result);
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        String space =" ";
        String result = soldiersCount + space + name;
        System.out.println(result);

       // END
    }
}
